# bloodsimple

A minimum-friction way for artists to sell their work online.

.
## Details

There are a million ways to sell things online, but all of them require artists to do varying amounts of non-artist things.  These systems and platforms focus on the business aspects of operating an online store, and provide many features that are advantagous to someone interested in running a business, but these features only get in the way for people who *don't* want to run an online business and only do so out of *necessity*.

I have many artist friends who create volumes of beautiful, inspirational work, and who are regularly asked to make their work avaliable for purchase online.  Those who have invest significant amounts of time doing so (both on an initial and ongoing basis) and the all lament how the time spent dealing with their online shop takes time away from creating art.  This seems illogical, if anything technology and automation should take *less* time away from the creative endevors humans are uniquely suited for, otherwise what's the point?

Bloodsimple exists to address this.  It is designed to put the artist first.  Any compromise is first absorbed by the system, and second by the patron (customer).  Only after all options to offload complexity to these two places has been exausted is it placed on the artist.  This runs contrary to most business philosopy **by design**.  The problem we are solving with bloodsimple is not the problem already solved by dozens (hundreds?) of existing online business packages which follow traditional business philosophy.  We are not trying to maximize margins, optimize cross-sells or scale production to meet the demand of future trends.  We are trying to remove all barriers to artists selling their work online, allowing them to do what they do best, **make art**.


## Stories

### Joe the artist

Joe has a pile of paintings in the corner of his room.  He could use some more supplies, and his friends have asked to buy his work, but he's tried setting up a website before and wasted a whole day doing that when he could have been drawing.  

Joe takes out his phone and goes to www.bloodsimple.com.  Since he's never been there before he's asked for his email address.  Joe types this in and taps "login".  Bloodsimple shows Joe a message that instructs him to go to his inbox, where he will find an email from bloodsimple with a link he can click to login.  Joe clicks the link and is shown a page with a big button that says "add art".

Joe taps "add art" and an instructional page appears:

*"since this is your first time, here's how this works: When you click "next", your camera will open to take a picture of the art you want to add.  Once you take a picture that you like, you can enter how much you want to get paid for the work, and the art will be added to your shop.  You can always go back and change the photo or the price.  Later you can add more photos, information or downloadable content.*

Joe taps "next", takes a picture of one of the drawings, enters a fee and is shown a list of items in his gallery (which currently contains only the one item).  

### Gallery Name
#### Artist Name
##### https://bloodsimple.com/gallery-name

| image | description |  fee   | delete |
|-------|-------------|--------|--------|
| photo | none        | $25.00 |   X    |

Joe adds a few more drawings, adds some descriptions and decides he's ready to share the gallery.  Joe copies the gallery's URL from the top of the page and posts it on Facebook and tags his friend Mary.

A few hours later, Joe gets an email from bloodsimple saying he's sold a piece of art.  The email includes a link to click when he's ready to tell the buyer (in this case, his friend Mary) when the piece will ship and when she should expect it to arrive.  Joe clicks the link and types a message to Mary that says he'll mail the drawing tomorrow and it should get to her in 2-5 days.


### Mary the patron

Mary clicks a link Joe posted on Facebook and is taken to Joe's bloodsimple gallery.

### Gallery Name
#### Artist Name
##### https://bloodsimple.com/gallery-name

| image | description | price  | buy |
|-------|-------------|--------|-----|
| photo | A dragon    | $26.28 |  $  |
| photo | A deer      | $15.89 |  $  |
| photo | A boat      | $47.06 |  $  |
| photo | A car       | $78.23 |  $  |

Mary clicks each piece in the gallery to check things out, but she's really only interested in the dragon.  On the dragon page she sees a larger photo of the dragon drawing, the complete description Joe entered and a "buy" button displaying the price of the art.  Mary clicks the "buy" button and is shown a form where she can enter her payment and delivery information.

Mary completes and submits the form, and moments later recieves an email reciept for her order.  Later, she receives a personalized response from Joe which includes shipping information (delivery date, tracking, etc.).


## FAQ

* Q: Why is it called "bloodsimple"?
* A: I didn't want to waste time thinking about names, and I misunderstood "blood simple" to mean "easy by nature".  Turns out it means [something else](), and will likely be changed sometime in the near future.

* Q: Why isn't there a shopping cart?
* A: It's assumed that each piece of art is a singular piece that can only be sold once.  If a piece were added to one patron's cart, would it still appear as avaliable to other shoppers, or would it be made unavaliable while it's in one person's cart?  Neither is desirable because in the former case you could place something in your cart only to have it disappear if someone else purchases it before you return.  Alternatively if the former were implemented a piece could be left in an abandonded cart forever and never avaliable for purchase by other patrons.  Until we can resolve these problems we can't add a cart.

* Q: How is shipping calculated?
* A: The artist includes shipping in the cost of the item.


## Open Questions

* How do the artists get their money?
  + Does bloodsimple use a single Stripe account to collect all funds and then distribute them to each artist?
  + Does each artist supply their own Stripe account info?
* How do patrons dispute a purchase?
* Does bloodsimple end up with any liability due to its role in the transaction?
* Will users be happy with passwordless, email-verification login?
  + How do we establish enough trust for the initial account creation?
* What records should be kept?
  + Transactions?
    + Sale history?
    + Purchase history?
  + Gallery contents history (purchased/deleted art)?
  + Visitor statistics (analytics)?
  + Patron information?
* What does the service fee need to be to cover expenses?
  + Stripe: .029 + .30
  + Example: .01
  + Total: .039 + .30
* How should an artist log back in when their session times-out, or they use a different computer, etc.?
* There is a federation play here, but I don't want to get distracted by it, when should we get into that?
